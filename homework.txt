The Story:
A long time ago in a galaxy far, far away... a talented imperial coder created
an application for the Death Star's Defense System which created some random
numbers, printed them to the screen, selected all primes from those numbers,
and printed them to the screen on a separate line.
Because of Top Secret reasons we need to build that amazing and very complex
application again on our brand new imperial computers, but both the original
library and the original author (it was an ugly and unfortunate accident) is
lost, we only have the source of the main.cpp and the (not too verbose)
documentation of the lost library.

The Task:
Create Util and NumberHandler classes and a CMakeLists.txt file, so main.cpp
is buildable and working. Do NOT modify main.cpp, because it was acknlowedged
in the current state by the Sith Lord himself, and we're afraid any
modification to that file would result in very painful experiences (and
additional ugly and unfortunate accidents).

The Help:
See attached Doxygen documentation ($PWD/html/index.html) for a (very) short
description of the class methods needed.
