#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <sstream>
#include <functional>
#include <algorithm>
#include <numeric>
#include <random>

class Util
{
public:
    Util() = delete;
    Util(const Util &) = delete;

    template<typename T>
    static T random(T min, T max)
    {
        std::random_device generator;
        std::uniform_int_distribution<T> distribution(min, max);

        return distribution(generator);
    }

    template<typename T>
    static bool isPrime(T number)
    {
        if(number < 2)
        {
            return false;
        }

        std::vector<T> numbersToTest;

        numbersToTest.resize(number / 2);
        std::iota(numbersToTest.begin(), numbersToTest.end(), 2);

        return !std::any_of(numbersToTest.cbegin(), numbersToTest.cend(), [number](const T& num){ return (number % num) == 0; } );
    }

    static std::string formatList(auto container)
    {
        return std::accumulate(container.cbegin(), container.cend(),  std::string{},
                                [](const std::string &acc, auto &curr)
                                {
                                    std::ostringstream a;
                                    a << acc << curr << " ";
                                    return a.str();
                                }
        );
    }
};

#endif // UTIL_H
