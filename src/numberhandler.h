#ifndef NUMBERHANDLER_H
#define NUMBERHANDLER_H

#include <functional>
#include <numeric>

template<typename T, typename CONTAINER>
class NumberHandler
{
public:
    NumberHandler() {}

    void fill(int count, std::function< T()> generator)
    {
        container.resize(container.size() + count);
        std::generate_n(container.begin(), count, std::ref(generator));
    }

    CONTAINER select(std::function< bool(T)> filter)
    {
        CONTAINER result(std::count_if(container.cbegin(), container.cend(), filter));

        std::copy_if(container.cbegin(), container.cend(), result.begin(), filter);

        return result;
    }

    const CONTAINER & list() const
    {
        return container;
    }

private:
    CONTAINER container;
};

#endif // NUMBERHANDLER_H
